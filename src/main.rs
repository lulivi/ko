//! Main CLI of KanOrg.
extern crate clap;

use clap::{App, Arg, SubCommand};
use std::{env, process};

use kanorg::board::{KanOrgBoard, KanOrgBoardResult};

/// Returns the argument matches from the command line
pub fn get_matches<'a>() -> clap::ArgMatches<'a> {
    let create_args = &[Arg::with_name("target_dir")
        .index(1)
        .help("Target directory to create the new config")];
    let show_args = &[Arg::with_name("task | workflow")
        .index(1)
        .help("Specific task or workflow to display")];
    let add_args = &[
        Arg::with_name("title")
            .index(1)
            .required(true)
            .help("Define the new task title"),
        Arg::with_name("workflow")
            .index(2)
            .help("Specify the initial task workflow"),
        Arg::with_name("edit")
            .short("e")
            .help("Edit the task once created"),
    ];
    let move_args = &[
        Arg::with_name("task")
            .index(1)
            .required(true)
            .help("Task identifier"),
        Arg::with_name("workflow")
            .index(2)
            .help("New workflow to which move the task to"),
    ];
    let edit_args = &[Arg::with_name("task")
        .index(1)
        .required(true)
        .help("Task identifier")];
    let delete_args = &[Arg::with_name("task")
        .index(1)
        .required(true)
        .help("Task identifier")];

    App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .subcommands(vec![
            SubCommand::with_name("create")
                .about("Create a default config in the current directory")
                .args(create_args),
            SubCommand::with_name("c")
                .about("Alias for `create` subcommand")
                .args(create_args),
            SubCommand::with_name("show")
                .about("Show the KanOrg display")
                .args(show_args),
            SubCommand::with_name("s")
                .about("Alias for `show` subcommand")
                .args(show_args),
            SubCommand::with_name("add")
                .about("Add a new task to the workflow")
                .args(add_args),
            SubCommand::with_name("a")
                .about("Alias for `add` subcommand")
                .args(add_args),
            SubCommand::with_name("move")
                .about("Move a task to another workflow")
                .args(move_args),
            SubCommand::with_name("m")
                .about("Alias for `move` subcommand")
                .args(move_args),
            SubCommand::with_name("edit")
                .about("Edit the title or description from a task")
                .args(edit_args),
            SubCommand::with_name("e")
                .about("Alias for `edit` subcommand")
                .args(edit_args),
            SubCommand::with_name("delete")
                .about("Delete a task from any workflow")
                .args(delete_args),
            SubCommand::with_name("d")
                .about("Alias for `delete` subcommand")
                .args(delete_args),
        ])
        .get_matches()
}

/// Collect errors form the subcommands and print them. Then exit the program.
fn print_error_and_exit(result: KanOrgBoardResult<()>) {
    if let Err(err) = result {
        println!("{}", err);
        process::exit(1);
    };
}

/// Main function.
fn main() -> KanOrgBoardResult<()> {
    let matches = get_matches();

    match matches.subcommand() {
        ("create", Some(subcommand)) | ("c", Some(subcommand)) => {
            print_error_and_exit(KanOrgBoard::create(
                subcommand.value_of("target_dir").unwrap_or("."),
            ));
        }
        _ => {
            let mut board = KanOrgBoard::new(&env::current_dir()?)?;

            match matches.subcommand() {
                ("show", Some(subcommand)) | ("s", Some(subcommand)) => {
                    print_error_and_exit(board.show(
                        subcommand.value_of("task | workflow"),
                        &mut std::io::stdout(),
                    ));
                }
                ("add", Some(subcommand)) | ("a", Some(subcommand)) => {
                    print_error_and_exit(board.add(
                        subcommand.value_of("title").unwrap(),
                        subcommand.value_of("workflow"),
                        subcommand.is_present("edit"),
                    ));
                }
                ("move", Some(subcommand)) | ("m", Some(subcommand)) => {
                    print_error_and_exit(board.relocate(
                        subcommand.value_of("task").unwrap(),
                        subcommand.value_of("workflow"),
                    ));
                }
                ("edit", Some(subcommand)) | ("e", Some(subcommand)) => {
                    print_error_and_exit(board.edit(subcommand.value_of("task").unwrap()));
                }
                ("delete", Some(subcommand)) | ("d", Some(subcommand)) => {
                    print_error_and_exit(board.delete(subcommand.value_of("task").unwrap()));
                }
                _ => println!("{}", matches.usage()),
            }
        }
    }

    Ok(())
}
