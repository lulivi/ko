//! KanOrg allows you easily organize all your tasks from the terminal.
//!
//! This commandline application will help you save everything important and will let you take the
//! control of your work (and not the other way arround).
//!
//! If you want to know more about the CLI commands, take a look to the [`KanOrgBoard`] struct.
//! 
//! [`KanOrgBoard`]: board::KanOrgBoard
pub mod board;
mod workflows;

/// Obtain the current terminal size.
///
/// As [`terminal_size::terminal_size()`] function doesn't return anything in docker containers
/// (used for gitlab tests), we need to return a default value: `80`.
fn get_terminal_width() -> usize {
    #[cfg(not(any(test, feature = "integration")))]
    if let Some((terminal_size::Width(w), _)) = terminal_size::terminal_size() {
        return w as usize;
    }
    80 as usize
}

/// Base KanOrg config directory.
const KO_BASE_DIR: &'static str = ".kanorg.d";
/// Configuration file name.
const KO_CFG_FILE: &'static str = "config";
/// Active tasks directory.
const KO_ACTIVE_TASKS_DIR: &'static str = "active.d";
/// Archived tasks directory.
const KO_ARCHIVED_TASKS_DIR: &'static str = "archive.d";
