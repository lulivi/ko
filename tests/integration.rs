extern crate kanorg;

use kanorg::board::KanOrgBoard;

mod integration_tests {
    use super::*;
    use std::path::PathBuf;

    const KO_BASE_DIR: &'static str = ".kanorg.d";
    const KO_CFG_FILE: &'static str = "config";
    const KO_ACTIVE_TASKS_DIR: &'static str = "active.d";
    const KO_ARCHIVED_TASKS_DIR: &'static str = "archive.d";

    fn setup_workspace(copy_example_config: bool) -> tempfile::TempDir {
        let temp_dir =
            tempfile::tempdir().expect("There was a problem creating test temporary file.");

        if copy_example_config {
            fs_extra::copy_items(
                &[PathBuf::from(env!("PWD"))
                    .join("examples")
                    .join(KO_BASE_DIR)],
                temp_dir.path(),
                &fs_extra::dir::CopyOptions::new(),
            )
            .expect("There was a problem copying test files.");
        }

        temp_dir
    }

    #[test]
    fn create_add_move_show_delete() {
        let tmpdir = setup_workspace(false);
        let temp_dir = tmpdir.path().to_path_buf();

        let base_dir = temp_dir.join(KO_BASE_DIR);
        let config_file = base_dir.join(KO_CFG_FILE);
        let active_tasks_dir = base_dir.join(KO_ACTIVE_TASKS_DIR);
        let archive_tasks_dir = base_dir.join(KO_ARCHIVED_TASKS_DIR);

        // --------------------------- //
        // Configuration tree creation //
        // ----------------------------//

        // First we wont have the configuration directory
        assert!(!base_dir.is_dir());

        assert!(KanOrgBoard::create(&temp_dir.to_str().unwrap()).is_ok());

        // Ensure everything is created
        assert!(config_file.is_file());
        assert!(active_tasks_dir.is_dir());
        assert!(archive_tasks_dir.is_dir());

        // ------------------ //
        // Board modification //
        // -------------------//

        // The new task file still does not exist
        assert!(!active_tasks_dir.join("1").is_file());

        let mut k = KanOrgBoard::new(&temp_dir).expect("There was an error creatint the board.");

        assert!(k.add("Hello, I'm a new task", Some("todo"), false).is_ok());
        assert!(k.add("Hello, I'm another task", None, false).is_ok());

        // The file is created without problems
        assert!(active_tasks_dir.join("1").is_file());

        // Move the tawk to other workflow
        assert!(k.relocate("1", Some("doing")).is_ok());

        // Delete a task
        assert!(k.delete("1").is_ok());

        // ---------------- //
        // Board displaying //
        // -----------------//

        let expected_output = "DOING\n";
        let mut stdout_capture = std::io::Cursor::new(Vec::new());

        assert!(k.show(Some("doing"), &mut stdout_capture).is_ok());
        let obtained_output = stdout_capture.into_inner();

        // Find the \n position in order to match the correct output and leave out the header
        let endline_pos = obtained_output.iter().position(|&e| e == 10u8).unwrap() as usize;

        assert_eq!(
            expected_output.as_bytes(),
            // Remove the header with the config directory
            &obtained_output[endline_pos+2..]
        );

        let expected_output = "\
        BACKLOG\n   \
        2 Hello, I'm another task\n\
        ";
        let mut stdout_capture = std::io::Cursor::new(Vec::new());

        assert!(k.show(Some("backlog"), &mut stdout_capture).is_ok());
        let obtained_output = stdout_capture.into_inner();

        // Find the \n position in order to match the correct output and leave out the header
        let endline_pos = obtained_output.iter().position(|&e| e == 10u8).unwrap() as usize;

        assert_eq!(
            expected_output.as_bytes(),
            // Remove the header with the config directory
            &obtained_output[endline_pos+2..]
        );
    }
}
