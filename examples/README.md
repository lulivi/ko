# Example configuration estructure

> :warning: if you are going to modify the `.kanorg.d` directory or its
> contents, ensure all the changes are gone before running `cargo test`, as this
> directory is used in the tests.

Here you can find the basic structure of the *KanOrg* configuration.

The main config file will be formatted in [TOML] format. It must contain the
variables *backlog*, *todo*, *doing*, *done* and *last_task*. For example [the
config file](./.kanorg.d/config) of this directory:

```toml
backlog = [25, 24, 23, 22, 21, 20, 19]
todo = [18, 17, 16, 15]
doing = [14, 13]
done = [12, 11, 10, 9, 8]
last_task = 25
```

To each workflow will be assigned a list of task IDs (can also be an empty
list). In adition, the last task ID is present to keep a counter. Inside the
`active.d/` directory, each task ID will have a file associated to it.
Following the previous config file example, we would have:

```shell
> tree --charset ascii -a
examples/
|-- .kanorg.d
|   |-- active.d
|   |   |-- 10
|   |   |-- 11
|   |   |-- 12
|   |   |-- 13
|   |   |-- 14
|   |   |-- 15
|   |   |-- 16
|   |   |-- 17
|   |   |-- 18
|   |   |-- 19
|   |   |-- 20
|   |   |-- 21
|   |   |-- 22
|   |   |-- 23
|   |   |-- 24
|   |   |-- 25
|   |   |-- 8
|   |   `-- 9
|   |-- archive.d
|   |   |-- 1
|   |   |-- 2
|   |   |-- 3
|   |   |-- 4
|   |   |-- 5
|   |   |-- 6
|   |   `-- 7
|   `-- config
`-- README.md

3 directories, 27 files
```

All files will have at least one line starting with the Markdown *h1* header
token (`#`). For example file `1` could contain:

```plain
# This is the task 1 sample title

some description from the task 1
```

The main workflows are named *todo*, *doing*, *done*. The *backlog* will be an
auxiliar workflow in which only 5 tasks will be shown. If you wan to list all
the tasks in a specific workflow, checkout the section [CLI](../README.md#cli)
from the root *README*.

One particularity of the *done* workflow is that it will only contain the **5**
last tasks that got there. When a new task is moved to the mentioned workflow,
the oldest one will be relocated to the `archive.d` directory and deleted from
the workflow variable.
