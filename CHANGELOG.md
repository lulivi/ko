# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - ReleaseDate

## [0.5.0] - 2021-05-20

### Added

- Added `suity` for running the tests.

### Modified

- By default the new tasks will not be edited. You will need to use the flag
  `-e` in order to edit the task inplace after creating it.
- Allowed to pass the task ID as an argument to the `show` subcommand.
- Restructured library classes and files. Levelled the field for future
  features.
- Moved from anyhow to custom errors.

## [0.4.0] - 2021-05-02

### Added

- Added edit command.
- Added delete command and its tests.

### Changed

- Reworked `Workflows` struct, adding all necessary methods to interact with
  them.
- Removed the feature from the binary and moved it to the integration tests to
  avoid calling the text editor during the tests.

## [0.3.1] - 2021-05-02

### Changed

- Fixed "no binary found" error when installing the crate.

## [0.3.0] - 2021-05-02

### Added

- Created alias for all the subcommands.
- Brand new changelog!
- Added `move` command logic and tests.

### Changed

- Modified `kanorg::get_terminal_width()` function in order to run properly the
  integration tests in docker containers.
- Reworked the [README.md](README.md) file.
- Reworked the GitLab pipeline.

## [0.2.0] - 2020-04-27

### Added

- Implemented basic subcommands: *create*, *add*, and *show*.
- Added Unit and Integration tests for the previously mentioned subcommands.
- Added README.md and [examples](./examples/) directory.

[Unreleased]: https://gitlab.com/lulivi/ko/-/compare/v0.5.0...main
[0.5.0]: https://gitlab.com/lulivi/ko/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/lulivi/ko/-/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/lulivi/ko/-/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/lulivi/ko/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/lulivi/ko/-/tree/v0.2.0
