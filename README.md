# KanOrg: kanban organizer

[![Crates.io](https://img.shields.io/crates/v/kanorg)][kanorg-crate]
[![docs.rs](https://docs.rs/kanorg/badge.svg)][kanorg-doc]
[![Pipeline](https://img.shields.io/gitlab/pipeline/lulivi/ko/main)][kanorg-pipeline]

> KanOrg allows you easily organize all your tasks from the terminal.

This commandline application will help you save everything important and will
let you take the control of your work (and not the other way around).

---

Table of contents:

<!-- markdownlint-disable MD030 -->
1. [Installation :floppy_disk:](#installation-floppy_disk)
2. [Before running anything, some basics :warning:](#before-running-anything-some-basics-warning)
3. [CLI :dvd:](#cli-dvd)
   1. [Create](#create)
   2. [Show](#show)
   3. [Add](#add)
   4. [Move](#move)
   5. [Edit](#edit)
   6. [Delete](#delete)
4. [Running tests :bomb:](#running-tests-bomb)
5. [License :bookmark:](#license-bookmark)
<!-- markdownlint-restore -->

---

## Installation :floppy_disk:

To install the package into `$HOME/.cargo/bin` run:

```shell
cargo install kanorg
```

## Before running anything, some basics :warning:

This CLI is based on a well defined directory structure. It is based on a main
directory `.kanorg.d`, the configuration file (`.kanorg.d/config`), and the
active and archived task directories (`.kanorg.d/active.d` and
`.kanorg.d/archive.d` respectively).

Take a look to the ["examples"](./examples/) directory where all the files
mentioned above can be found. By default, when you create a new *KanOrg* config
directory, you will have empty active and archive directories with empty lists
of tasks in the config file.

## CLI :dvd:

In this section some use cases of each command will be mentioned. You can also
check an API [documentation][kanorg-doc] of the library. If you want to see the
documentation of private structs and methods, you will need to run the following
after cloning the repo:

```shell
cargo doc --document-private-items --open
```

### Create

To create a new configuration environment you can run:

```shell
ko c[reate] [<target directory>]
```

By default, if you don't pass the `<target directory>` argument, it will create
the configuration in the current directory.

### Show

To show the active tasks you can run:

```shell
ko s[how] [<task | workflow>]
```

If no workflow or task is specified you will get something like:

```plain
$ ko show

|          TODO           |          DOING          |          DONE           |
|-------------------------|-------------------------|-------------------------|
|  18 This is the task 18 |  14 This is the task 14 |  12 This is the task 12 |
|  17 This is the task 17 |  13 This is the task 13 |  11 This is the task 11 |
|  16 This is the task 16 |                         |  10 This is the task 10 |
|  15 This is the task 15 |                         |   9 This is the task 9  |
|                         |                         |   8 This is the task 8  |

BACKLOG
  25 This is the task 25 sample title
  24 This is the task 24 sample title
  23 This is the task 23 sample title
  22 This is the task 22 sample title
  21 This is the task 21 sample title
WARNING: The backlog has been trimmed. Run `ko show backlog` to see all the backlog tasks.
```

You can also specify some workflow to list all the tasks in it. It will show
something like:

```plain
$ ko show backlog

BACKLOG
  25 This is the task 25 sample title
  24 This is the task 24 sample title
  23 This is the task 23 sample title
  22 This is the task 22 sample title
  21 This is the task 21 sample title
  20 This is the task 20 sample title
  19 This is the task 19 sample title
```

On the other hand, if you pass the ID of a task, it will show its contets:

```plain
$ ko show 17

# This is the task 17 sample title

some description from the task 17

```

### Add

You can add a new task with:

```shell
ko a[dd] [-e] <task title> [<workflow name>]
```

If the `<workflow name>` is not specified, the new task will be created in
*backlog*. If you want to edit the task in place, after creating it, use the
flag `-e`.

### Move

In order to move a task from one workflow to another:

```shell
ko m[ove] <task ID> [<workflow name>]
```

If no `<workflow name>` is specified, it will be moved to backlog.

### Edit

You can edit any active task running:

```shell
ko e[dit] <task ID>
```

It will open the task file with your default editor (`$EDITOR` environment
variable). If no editor is defined, it will try to use `vi`.

### Delete

Delete an active task:

```shell
ko d[elete] <task ID>
```

## Running tests :bomb:

In order to run the package tests:

```shell
cargo test [(--lib | --tests integration --features integration)]
```

## License :bookmark:

The source code is released under the [MIT license](./LICENSE).

[TOLM]: https://toml.io/en/
[kanorg-crate]: https://crates.io/crates/kanorg
[kanorg-doc]: https://docs.rs/kanorg/
[kanorg-pipeline]: https://gitlab.com/lulivi/ko/-/pipelines/
